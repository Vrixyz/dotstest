using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;

partial class ApplyVelocitySystem : SystemBase
{
    protected override void OnUpdate()
    {
        float deltaTime = Time.DeltaTime;
        Entities
            .ForEach((Entity entity, ref PhysicsVelocity vel, in Target target) =>
            {
                Translation translation = GetComponent<Translation>(entity);

                if (target.entity == Entity.Null)
                {
                    return;
                }
                float3 offset = GetComponent<Translation>(target.entity).Value - translation.Value;
                float3 targetVel = math.normalize(offset) * 20f;
                vel.Linear = targetVel;
                //                SetComponent<Velocity>(entity, new Velocity { Angular = float3.zero, Linear = targetVel });
            })
            .Schedule();
    }
}