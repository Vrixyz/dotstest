// Authoring component
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

class TargetAuthoring : MonoBehaviour
{
    public float speed;
}

// Runtime component
struct Target : IComponentData
{
    public Entity entity;
}
// Runtime component
struct Speed : IComponentData
{
    public float Value;
}
struct SearchForTarget : IComponentData
{
    public int useless;
}

// Conversion system, running in the conversion world
class FooConversion : GameObjectConversionSystem
{
    protected override void OnUpdate()
    {
        // Iterate over all authoring components of type FooAuthoring
        Entities.ForEach((TargetAuthoring input) =>
        {
            // Get the destination world entity associated with the authoring GameObject
            var entity = GetPrimaryEntity(input);

            // Do the conversion and add the ECS components

            DstEntityManager.AddComponentData(entity, new Target
            {
                entity = Entity.Null
            });
            DstEntityManager.AddComponentData(entity, new Speed
            {
                Value = input.speed
            });
            DstEntityManager.AddComponentData(entity, new SearchForTarget
            {
                useless = 1
            });
        });
    }
}

// SearchTarget system, running in the normal world
partial class SearchTarget : SystemBase
{
    Entity currentTarget = Entity.Null;
    EntityQuery queryPlayer;

    protected override void OnCreate()
    {
        queryPlayer = GetEntityQuery(typeof(Player));
    }

    protected override void OnUpdate()
    {
        Debug.Log(currentTarget);
        if (currentTarget == Entity.Null)
        {
            if (queryPlayer.IsEmpty)
            {
                return;
            }
            currentTarget = queryPlayer.ToEntityArray(Allocator.TempJob)[0];
            return;
        }
        if (!EntityManager.Exists(currentTarget))
        {
            currentTarget = Entity.Null;
            return;
        }
        EntityCommandBuffer ecb = new EntityCommandBuffer(Allocator.TempJob);
        Entity localCurrentTarget = currentTarget;
        // Iterate over all authoring components of type FooAuthoring
        Entities.ForEach((Entity entity, ref Target target, in SearchForTarget input) =>
        {
            target.entity = localCurrentTarget;
            ecb.RemoveComponent<SearchForTarget>(entity);
        }).Schedule();
        this.Dependency.Complete();
        ecb.Playback(this.EntityManager);
        ecb.Dispose();
    }
}