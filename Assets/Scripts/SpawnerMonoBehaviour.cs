using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class SpawnerMonoBehaviour : MonoBehaviour
{
    [SerializeField]
    GameObject prefab;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(prefab);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            World.DefaultGameObjectInjectionWorld.EntityManager.DestroyAndResetAllEntities();
        }
    }
}
