using Unity.Entities;
using UnityEngine;

class PlayerAuthoring : MonoBehaviour
{
    public float speed;
}

// Runtime component
struct Player : IComponentData
{

}

// Conversion system, running in the conversion world
class PlayerConversion : GameObjectConversionSystem
{
    protected override void OnUpdate()
    {
        // Iterate over all authoring components of type FooAuthoring
        Entities.ForEach((PlayerAuthoring input) =>
        {
            // Get the destination world entity associated with the authoring GameObject
            var entity = GetPrimaryEntity(input);

            // Do the conversion and add the ECS components

            DstEntityManager.AddComponentData(entity, new Player());
        });
    }
}